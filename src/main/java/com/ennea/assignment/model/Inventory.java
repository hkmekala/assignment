package com.ennea.assignment.model;


import javax.persistence.*;

import lombok.*;
import org.joda.time.DateTime;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Inventory {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(length = 255)
    private String code;

    @Column(length = 255)
    private String name;

    @Column(length = 255)
    private String batch;

    private Integer stock;
    private Integer deal;
    private Integer free;
    private Float mrp;
    private Float rate;


    private Date exp;

    @Column(length = 255)
    private String company;

    @Column(length = 255)
    private String supplier;


    @CreatedDate
    private DateTime createdAt;

    @LastModifiedDate
    private DateTime updatedAt;
}
