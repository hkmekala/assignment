package com.ennea.assignment.request;

public class InventoryPayload {
    public String batch;
    public String code;
    public String company;
    public String deal;
    public String exp;
    public String free;
    public String mrp;
    public String name;
    public String rate;
    public String stock;
    public String supplier;
}
