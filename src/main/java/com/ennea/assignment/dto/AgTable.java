package com.ennea.assignment.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class AgTable {
    private String name;
    private String batch;
    private Integer stock;
    private Float mrp;
    private Float rate;
    private Date exp;
    private Integer free;
    private Integer deal;
}
