package com.ennea.assignment.dto;

public class GenericResponse {
    public Boolean status;
    public Object data;
}
