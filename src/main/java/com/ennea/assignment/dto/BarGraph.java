package com.ennea.assignment.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BarGraph {
    private String name;
    private Integer expCount;
}
