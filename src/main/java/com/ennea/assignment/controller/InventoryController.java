package com.ennea.assignment.controller;


import com.ennea.assignment.dto.AgTable;
import com.ennea.assignment.dto.BarGraph;
import com.ennea.assignment.dto.GenericResponse;
import com.ennea.assignment.dto.PieReport;
import com.ennea.assignment.model.Inventory;
import com.ennea.assignment.requests.BarGraphRequest;
import com.ennea.assignment.requests.PieChartRequest;
import com.ennea.assignment.service.InventoryServicesImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/")
@CrossOrigin(origins = "http://localost:3000")
public class InventoryController {
    @Autowired
    private InventoryServicesImpl inventoryServices;

    @GetMapping("inventory")
    public ResponseEntity<GenericResponse> getInventory() {
        GenericResponse gr = new GenericResponse();
        gr.status = true;
        gr.data = inventoryServices.getInventory();

        return new ResponseEntity<GenericResponse> (gr, HttpStatus.OK);
    }


    @GetMapping("inventory/report-1")
    public ResponseEntity<GenericResponse> getTabularReport() {
        GenericResponse gr = new GenericResponse();
        gr.status = true;
        gr.data = "";

        return new ResponseEntity<GenericResponse>(gr, HttpStatus.OK);
    }

    @PostMapping("inventory/pie-chart")
    public ResponseEntity<GenericResponse> getPieChart(@RequestBody PieChartRequest pieChartRequest){

        PieReport pieReport = null;
        if(pieChartRequest.companyName != null && !pieChartRequest.companyName.isBlank())
        {
            pieReport = inventoryServices.getPieReportForCompany(pieChartRequest.companyName);
        }

        GenericResponse gr = new GenericResponse();
        gr.status = true;
        gr.data = pieReport;

        return new ResponseEntity<GenericResponse>(gr, HttpStatus.OK);
    }

    @PostMapping("inventory/bar-graph")
    public ResponseEntity<GenericResponse> getBarChart(@RequestBody BarGraphRequest barGraphRequest) {
        List<BarGraph> barGraph = null;
        if(barGraphRequest.companyName != null && barGraphRequest.date != null) {
            barGraph = inventoryServices.getBarGraphForCompany(barGraphRequest.date, barGraphRequest.companyName);
        }

        GenericResponse gr = new GenericResponse();
        gr.status = true;
        gr.data = barGraph;

        return  new ResponseEntity<GenericResponse>(gr, HttpStatus.OK);
    }

    @GetMapping("inventory/ag-table")
    public ResponseEntity<GenericResponse> getAgTable() {
        List<AgTable> agTables = inventoryServices.getInventoryTable();

        GenericResponse gr = new GenericResponse();
        gr.status = true;
        gr.data = agTables;

        return new ResponseEntity<GenericResponse>(gr, HttpStatus.OK);
    }
}
