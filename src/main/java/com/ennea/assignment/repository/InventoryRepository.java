package com.ennea.assignment.repository;

import com.ennea.assignment.model.Inventory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface InventoryRepository extends JpaRepository<Inventory, Long> {


    @Query(value = "SELECT company , SUM(case when (NOW() > Date(`exp`)) then 1 else 0 end) as expired, sum(case when (now() <= Date(`exp`)) then 1 else 0 end) as valid from inventory where company = ?1", nativeQuery = true)
    PieReport getPieReportForCompany(String companyName);

    public interface PieReport {
        String getCompany();
        Integer getExpired();
        Integer getValid();
    }


    @Query(value = "SELECT name, SUM(case when ( ?1 > Date(`exp`)) then 1 else 0 end ) as expCount from inventory where company  = ?2 GROUP by name", nativeQuery = true)
    List<BarGraph> getBarGraphForCompany(String date, String companyName);

    public interface BarGraph {
        String getName();
        Integer getExpCount();
    }

    @Query(value= "SELECT  name, \"all\", sum(stock) as stock, max(mrp) as mrp, max(rate) as rate, min(exp) as exp, free, deal  from inventory group by name", nativeQuery = true)
    List<AgTable> getAgTable();

    public interface AgTable {
        String getName();
        String getBatch();
        Integer getStock();
        Float getMrp();
        Float getRate();
        Date getExp();
        Integer getFree();
        Integer getDeal();
    }

}
