package com.ennea.assignment.service;

import com.ennea.assignment.dto.AgTable;
import com.ennea.assignment.dto.BarGraph;
import com.ennea.assignment.dto.PieReport;
import com.ennea.assignment.iservice.IInventoryService;
import com.ennea.assignment.model.Inventory;
import com.ennea.assignment.repository.InventoryRepository;
import com.ennea.assignment.request.InventoryPayload;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class InventoryServicesImpl implements IInventoryService {


    @Autowired
    private InventoryRepository inventoryRepository;

    public List<Inventory> getInventory() {
        return inventoryRepository.findAll();
    }

    @Override
    public List<AgTable> getInventoryTable() {

        List<InventoryRepository.AgTable> table = inventoryRepository.getAgTable();
        List<AgTable> responseTable = new ArrayList<AgTable>();
        table.forEach(t -> {
            AgTable tbo = new AgTable();
            tbo.setName(t.getName());
            tbo.setBatch(t.getBatch());
            tbo.setStock(t.getStock());
            tbo.setMrp(t.getMrp());
            tbo.setRate(t.getRate());
            tbo.setExp(t.getExp());
            tbo.setFree(t.getFree());
            tbo.setDeal(t.getDeal());

            responseTable.add(tbo);
        });

        return responseTable;
    }

    public Boolean bulkStoreInventory(List<InventoryPayload> inventoryPayloadList) {
        List<Inventory> inventories = new ArrayList<Inventory>();
        inventoryPayloadList.forEach(inventoryPayload -> {
            Inventory inventory = new Inventory();
            inventory.setBatch(inventoryPayload.batch);
            inventory.setCode(inventoryPayload.code);
            inventory.setCompany(inventoryPayload.company);
            inventory.setDeal(Integer.valueOf(inventoryPayload.deal));
            DateTimeFormatter formatter = DateTimeFormat.forPattern("d-MM-yyyy");
            DateTime expiriyDate = formatter.parseDateTime(inventoryPayload.exp);
            inventory.setExp(expiriyDate.toDate());
            inventory.setFree(Integer.valueOf(inventoryPayload.free));
            inventory.setMrp(Float.valueOf(inventoryPayload.mrp));
            inventory.setName(inventoryPayload.name);
            inventory.setRate(Float.valueOf(inventoryPayload.rate));
            inventory.setStock(Integer.valueOf(inventoryPayload.stock));
            inventory.setSupplier(inventoryPayload.supplier);

            inventories.add(inventory);
        });

        inventoryRepository.saveAll(inventories);

        return true;
    }

    @Override
    public PieReport getPieReportForCompany(String companyName) {
        InventoryRepository.PieReport pieReport = null;

        if(companyName != null && !companyName.isBlank()) {
            pieReport = inventoryRepository.getPieReportForCompany(companyName);
        }

        PieReport pieReportObject = new PieReport();
        pieReportObject.setCompany(pieReport.getCompany());
        pieReportObject.setValid(pieReport.getValid());
        pieReportObject.setExpired(pieReport.getExpired());

        return pieReportObject;
    }

    @Override
    public List<BarGraph> getBarGraphForCompany(String date, String companyName) {
        List<InventoryRepository.BarGraph> barGraph = null;

        if(companyName != null && date != null) {
            barGraph = inventoryRepository.getBarGraphForCompany(date, companyName);
        }

        List<BarGraph> barGraphList = new ArrayList<BarGraph>();

        barGraph.forEach(bg -> {
            BarGraph barGraphObject = new BarGraph();
            barGraphObject.setName(bg.getName());
            barGraphObject.setExpCount(bg.getExpCount());
            barGraphList.add(barGraphObject);
        });


        return barGraphList;
    }

    public Boolean storeInventory(InventoryPayload inventoryPayload) {
        try {
            Inventory inventory = new Inventory();
            inventory.setBatch(inventoryPayload.batch);
            inventory.setCode(inventoryPayload.code);
            inventory.setCompany(inventoryPayload.company);
            inventory.setDeal(Integer.valueOf(inventoryPayload.deal));
            DateTimeFormatter formatter = DateTimeFormat.forPattern("d-MM-yyyy");
            DateTime expiriyDate = formatter.parseDateTime(inventoryPayload.exp);
            inventory.setExp(expiriyDate.toDate());
            inventory.setFree(Integer.valueOf(inventoryPayload.free));
            inventory.setMrp(Float.valueOf(inventoryPayload.mrp));
            inventory.setName(inventoryPayload.name);
            inventory.setRate(Float.valueOf(inventoryPayload.rate));
            inventory.setStock(Integer.valueOf(inventoryPayload.stock));
            inventory.setSupplier(inventoryPayload.supplier);
//            inventory.setCreatedAt(DateTime.now());
//            inventory.setUpdatedAt(DateTime.now());
            inventoryRepository.save(inventory);
        } catch (NumberFormatException e) {
            return  false;
        }
        return true;
    }
}
