package com.ennea.assignment.config;

import com.ennea.assignment.iservice.IInventoryService;
import com.ennea.assignment.request.InventoryPayload;
import com.ennea.assignment.service.InventoryServicesImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@CrossOrigin(origins = "http://localost:3000")
public class WebSocketHandler extends TextWebSocketHandler {

    private static final Logger logger = LoggerFactory.getLogger(WebSocketHandler.class);

    @Autowired
    IInventoryService inventoryServices;


    public void handleTextMessage(WebSocketSession session, TextMessage message) throws InterruptedException, IOException {
        String payload = message.getPayload();
        logger.info(payload);
        ObjectMapper objectMapper = new ObjectMapper();
        InventoryPayload[] inventoryPayloadArr = objectMapper.readValue(payload, InventoryPayload[].class);
        List<InventoryPayload> inventoryPayloadList = new ArrayList<InventoryPayload>(List.of(inventoryPayloadArr));
        inventoryServices.bulkStoreInventory(inventoryPayloadList);
//        Boolean status = inventoryServices.storeInventory(inventoryPayload);
        Map<String, Object> response = new HashMap<String, Object>();
        response.put("status", true);
        response.put("msg", "Generic Message Place holder to be put here.");
        session.sendMessage(new TextMessage(response.toString()));
    }
}
