package com.ennea.assignment.iservice;

import com.ennea.assignment.dto.AgTable;
import com.ennea.assignment.dto.BarGraph;
import com.ennea.assignment.dto.PieReport;
import com.ennea.assignment.model.Inventory;
import com.ennea.assignment.request.InventoryPayload;

import java.util.List;

public interface IInventoryService {

    public List<Inventory> getInventory();
    public List<AgTable> getInventoryTable();
    public Boolean storeInventory(InventoryPayload inventoryPayload);
    public Boolean bulkStoreInventory(List<InventoryPayload> inventoryPayloadList);
    public PieReport getPieReportForCompany(String companyName);
    public List<BarGraph> getBarGraphForCompany(String date, String companyName);

}
